//This is only because the project source files are put within the net\bluespurs\db folders within the project directory

// This keeps my files talking to each other
package net.bluespurs.com;

//It needs this since it needs to understand how to display the data to us
//and to do so needs this class to convert the MySQL data to java data
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.sql.RowSetListener;
import javax.sql.rowset.CachedRowSet;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;


public class InventoryTable implements TableModel {
	static CachedRowSet empRowSet; // The ResultSet to interpret
	ResultSetMetaData metadata; // Additional information about the results
	int numcols, numrows; // How many rows and columns in the table

	public InventoryTable(CachedRowSet crs) throws SQLException {
		this.empRowSet = crs;
		this.metadata = this.empRowSet.getMetaData();
		numcols = metadata.getColumnCount();

		this.empRowSet.beforeFirst();
		this.numrows = 0;
		while (this.empRowSet.next()) {
			this.numrows++;
		}
		this.empRowSet.beforeFirst();
	}

	public CachedRowSet getCourseRowSet() {
		return empRowSet;
	}

	public static void addEventHandlersToRowSet(RowSetListener listener) {
		empRowSet.addRowSetListener(listener);
	}

	@Override
	public int getRowCount() {
		return numrows;
	}

	@Override
	public int getColumnCount() {
		return numcols;
	}

	@Override
	public String getColumnName(int columnIndex) {
		try {
			return this.metadata.getColumnLabel(columnIndex + 1);
		} catch (SQLException e) {
			return e.toString();
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return String.class;
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		try {
			this.empRowSet.absolute(rowIndex + 1);
			Object o = this.empRowSet.getObject(columnIndex + 1);
			if (o == null)
				return null;
			else
				return o.toString();
		} catch (SQLException e) {
			return e.toString();
		}
	}
	// this updates the CRS.
	public void insertRow(int productSku, String productName, String productBrand, int productQuantity) throws SQLException {

		try {
			this.empRowSet.moveToInsertRow();
			this.empRowSet.updateInt("PRODUCT_SKU", productSku);
			this.empRowSet.updateString("PRODUCT_NAME", productName);
			this.empRowSet.updateString("PRODUCT_BRAND", productBrand);
			this.empRowSet.updateInt("PRODUCT_QUANTITY", productQuantity);
			this.empRowSet.insertRow();
			this.empRowSet.moveToCurrentRow();
		} catch (SQLException e) {
			DatabaseEngine.printSQLException(e);
		}
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addTableModelListener(TableModelListener l) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
		// TODO Auto-generated method stub

	}

}