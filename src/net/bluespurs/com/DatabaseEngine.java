//This is only because the project source files are put within the net\bluespurs\db folders within the project directory

// This keeps my files talking to each other
package net.bluespurs.com;

//This is needed since the program will be attempting a connection with the database
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;//Used to set the properties of the username and password and send that off to the server

import javax.sql.rowset.CachedRowSet;//Used for the results set

import com.sun.rowset.CachedRowSetImpl;//Used for the results set

public class DatabaseEngine {

	//This gives main the information needed to start the myDatabase connection
	private static final String DB_URL = "jdbc:mysql://localhost:3306/Kristians_Database?autoReconnect=true&relaxAutoCommit=true";
	private static Connection connOne;
	public String userName;
	public String password;
	public String dbms;

	private DatabaseEngine() {
		//Trys the connection for the database at this point in the processing of the program
		try {

			Class.forName("com.mysql.jdbc.Driver");
			connOne = DriverManager.getConnection(DB_URL, getDatabaseSettings());
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private Properties getDatabaseSettings() {
		Properties properties = new Properties();
		properties.put("user", "root");//This is giving the username to the database
		properties.put("password", "");//This is giving the password to the database
		this.userName = properties.getProperty("user");//This will return the username
		this.password = properties.getProperty("password");//This will return the password
		this.dbms = "mysql";//Sets into place the database we will now be using being MySQL

		return properties;
	}

	//This sets a new instance of the database engine class
	private static class DatabaseEngineHelper {
		private static final DatabaseEngine INSTANCE = new DatabaseEngine();
	}

	//This retrieves the instance of the database engine class when we ask for it back
	public static DatabaseEngine getInstance() {
		return DatabaseEngineHelper.INSTANCE;
	}

	//Used for the results, and will throw an exception if they are not to be found
	private CachedRowSet CacheRowSet(ResultSet rs) throws SQLException {
		CachedRowSetImpl crs = new CachedRowSetImpl();
		crs.populate(rs);
		return crs;
	}

	public CachedRowSet selectAll() { // pretty print of Select all for SUBS
										// table
		String sql = "SELECT * FROM bluespurs_inventory;";
		Statement statement = null;

		try {

			statement = connOne.createStatement();
			ResultSet rset = statement.executeQuery(sql);
			CachedRowSet crs = CacheRowSet(rset);

			// while(crs.next()){
			// System.out.println("ID: " + crs.getInt(1) + ", LastName: " +
			// crs.getString(2) + ", FirstName: " + crs.getString(3));
			// }

			return crs;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	public static Connection getConnection() {
		return connOne;
	}

	public void setConnection(Connection connection) {
		DatabaseEngine.connOne = connection;
	}

	public static void printSQLException(SQLException ex) {
		for (Throwable e : ex) {
			if (e instanceof SQLException) {
				if (ignoreSQLException(((SQLException) e).getSQLState()) == false) {
					e.printStackTrace(System.err);
					System.err.println("SQLState: " + ((SQLException) e).getSQLState());
					System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
					System.err.println("Message: " + e.getMessage());
					Throwable t = ex.getCause();
					while (t != null) {
						System.out.println("Cause: " + t);
						t = t.getCause();
					}
				}
			}
		}
	}

	public static boolean ignoreSQLException(String sqlState) {
		if (sqlState == null) {
			System.out.println("The SQL state is not defined!");
			return false;
		}
		// X0Y32: Jar file already exists in schema
		if (sqlState.equalsIgnoreCase("X0Y32"))
			return true;
		// 42Y55: Table already exists in schema
		if (sqlState.equalsIgnoreCase("42Y55"))
			return true;
		return false;
	}
}
