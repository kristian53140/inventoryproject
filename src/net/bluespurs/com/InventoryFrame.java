//This is only because the project source files are put within the net\bluespurs\db folders within the project directory

// This keeps my files talking to each other
package net.bluespurs.com;

import java.awt.*;
import java.awt.event.*;

import java.sql.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.RowSetEvent;
import javax.sql.RowSetListener;
import javax.sql.rowset.CachedRowSet;
import javax.swing.*;

import com.sun.rowset.CachedRowSetImpl;



public class InventoryFrame extends JFrame implements RowSetListener {
	// setting up variables
	CachedRowSet crs;
	InventoryTable empTable;
	JTable table;
	Connection connection;
	
	// setting up labels and text fields for the frame.
	JLabel label_PRODUCT_SKU;
	JLabel label_PRODUCT_NAME;
	JLabel label_PRODUCT_BRAND;
	JLabel label_PRODUCT_QUANTITY;

	JTextField textField_PRODUCT_SKU;
	JTextField textField_PRODUCT_NAME;
	JTextField textField_PRODUCT_BRAND;
	JTextField textField_PRODUCT_QUANTITY;
	
	// buttons!
	JButton button_ADD_ROW;
	JButton button_DELETE_ROW;
	JButton button_UPDATE_TABLE;
	DatabaseEngine settings;

	public InventoryFrame(DatabaseEngine crs) throws SQLException { // constructor
		this.settings = crs;
		connection = DatabaseEngine.getConnection();
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {

				try {
					connection.close();
				} catch (SQLException sqle) {
					DatabaseEngine.printSQLException(sqle);
				}
				System.exit(0);
			}
		});

		// initialize stuffs

		CachedRowSet empCRS = getContentsOfCourseTable();  // puts the contents of the DB query into a CachedRowSet
		this.empTable = new InventoryTable(empCRS);  // Sets up a new table with the contents from db.
		InventoryTable.addEventHandlersToRowSet(this);  //this does stuff.

		// Table setup for frame
		table = new JTable();
		table.setModel(empTable);

		label_PRODUCT_SKU = new JLabel();
		label_PRODUCT_NAME = new JLabel();
		label_PRODUCT_BRAND = new JLabel();
		label_PRODUCT_QUANTITY = new JLabel();

		button_ADD_ROW = new JButton();
		button_DELETE_ROW = new JButton();
		button_UPDATE_TABLE = new JButton();

		textField_PRODUCT_SKU = new JTextField(10);
		textField_PRODUCT_NAME = new JTextField(10);
		textField_PRODUCT_BRAND = new JTextField(10);
		textField_PRODUCT_QUANTITY = new JTextField(10);

		label_PRODUCT_SKU.setText("Product SKU");
		label_PRODUCT_NAME.setText("Product Name");
		label_PRODUCT_BRAND.setText("Product Brand");
		label_PRODUCT_QUANTITY.setText("Product Quantity");

		button_ADD_ROW.setText("Add a Row");
		button_DELETE_ROW.setText("Delete a Row");
		button_UPDATE_TABLE.setText("Update the Table");

		Container contentPane = getContentPane();
		contentPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		contentPane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		// setup of table labels and boxes.
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.CENTER;
		c.weightx = 0.5;
		c.weighty = 1.0;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		contentPane.add(new JScrollPane(table), c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 0.25;
		c.weighty = 0;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 1;
		contentPane.add(label_PRODUCT_SKU, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_END;
		c.weightx = 0.75;
		c.weighty = 0;
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 1;
		contentPane.add(textField_PRODUCT_SKU, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.25;
		c.weighty = 0;
		c.anchor = GridBagConstraints.LINE_START;
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		contentPane.add(label_PRODUCT_NAME, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_END;
		c.weightx = 0.75;
		c.weighty = 0;
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 1;
		contentPane.add(textField_PRODUCT_NAME, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 0.25;
		c.weighty = 0;
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 1;
		contentPane.add(label_PRODUCT_BRAND, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_END;
		c.weightx = 0.75;
		c.weighty = 0;
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 1;
		contentPane.add(textField_PRODUCT_BRAND, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 0.25;
		c.weighty = 0;
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 1;
		contentPane.add(label_PRODUCT_QUANTITY, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_END;
		c.weightx = 0.75;
		c.weighty = 0;
		c.gridx = 1;
		c.gridy = 4;
		c.gridwidth = 1;
		contentPane.add(textField_PRODUCT_QUANTITY, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 0.5;
		c.weighty = 0;
		c.gridx = 0;
		c.gridy = 6;
		c.gridwidth = 1;
		contentPane.add(button_ADD_ROW, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_END;
		c.weightx = 0.5;
		c.weighty = 0;
		c.gridx = 1;
		c.gridy = 6;
		c.gridwidth = 1;
		contentPane.add(button_DELETE_ROW, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.LINE_START;
		c.weightx = 0.5;
		c.weighty = 0;
		c.gridx = 0;
		c.gridy = 7;
		c.gridwidth = 1;
		contentPane.add(button_UPDATE_TABLE, c);

		// Click handler for the Add Row button. This pops what you've got from the text boxes into the CRS and is displayed in the table.
		button_ADD_ROW.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				JOptionPane.showMessageDialog(InventoryFrame.this,
						new String[] { "Adding the following row:",
								"Product SKU: [" + textField_PRODUCT_SKU.getText().trim() + "]",
								"Product Name: [" + textField_PRODUCT_NAME.getText() + "]",
								"Product Brand: [" + textField_PRODUCT_BRAND.getText() + "]",
								"Product Quantity: [" + textField_PRODUCT_QUANTITY.getText().trim() + "]"});

				try {

					empTable.insertRow
							(Integer.parseInt(textField_PRODUCT_SKU.getText().trim()),
							(textField_PRODUCT_NAME.getText()),
							(textField_PRODUCT_BRAND.getText()),
							Integer.parseInt(textField_PRODUCT_QUANTITY.getText().trim()));
					
				} catch (SQLException sqle) {
					displaySQLExceptionDialog(sqle);
				}
				
				try {
					empTable.getCourseRowSet().acceptChanges();
					
				} catch (SQLException sqle) {
					displaySQLExceptionDialog(sqle);
					// Now revert back changes
					try {
						createNewTableModel();
					} catch (SQLException sqle2) {
						displaySQLExceptionDialog(sqle2);
					}
				}
				
			}
		});

		// Click Handler for the Update Button. This takes the CRS and pushes it to the database. with crs.acceptChanges();
		button_DELETE_ROW.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				//System.out.println("I do nothing at this point in time");
				
				try {
					Connection dbConnection = null;
					PreparedStatement preparedStatement = null;
				
					String deleteSQL = "DELETE FROM bluespurs_inventory WHERE PRODUCT_SKU = 1998;";
					preparedStatement = dbConnection.prepareStatement(deleteSQL);
					preparedStatement.executeUpdate();
				}
				
				catch (SQLException x) {
					
					System.out.println(x.getMessage());
					
				}
				
				
			}
		});
		
		//Click Handler for Discard button. This removes the pending changes that you have in the table. by creating a new Table Model(re getting the stuff from the DB)
		button_UPDATE_TABLE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					//rs.deleteRow();
					createNewTableModel();
				} catch (SQLException sqle) {
					displaySQLExceptionDialog(sqle);
				}
			}
		});

	}
	
	private void createNewTableModel() throws SQLException {
		empTable = new InventoryTable(getContentsOfCourseTable());
		InventoryTable.addEventHandlersToRowSet(this);
		table.setModel(empTable);
	}
	
	// funky method to dislpay SQL exception in a fancy-pants pop up window.
	private void displaySQLExceptionDialog(SQLException e) {

		// Display the SQLException in a dialog box
		JOptionPane.showMessageDialog(InventoryFrame.this,
				new String[] { e.getClass().getName() + ": ", e.getMessage() });
	}



	
	// Re-Gets contents of DB query. This was a huge pain. After every call the connection is dropped and you're required to re-connect.
	public CachedRowSet getContentsOfCourseTable() throws SQLException {
		CachedRowSet crs = null;
	    try {
	      connection = DatabaseEngine.getConnection();
	      crs = new CachedRowSetImpl();
	      crs.setType(ResultSet.TYPE_SCROLL_INSENSITIVE);
	      crs.setConcurrency(ResultSet.CONCUR_UPDATABLE);
	      crs.setUsername(settings.userName);
	      crs.setPassword(settings.password);
	      // In MySQL, to disable auto-commit, set the property relaxAutoCommit to
	      // true in the connection URL.
	      crs.setUrl("jdbc:mysql://localhost:3306/Kristians_Database?autoReconnect=true&relaxAutoCommit=true");
	      
	      crs.setCommand("SELECT * FROM bluespurs_inventory");
	      crs.execute();

	    } catch (SQLException e) {
	      DatabaseEngine.printSQLException(e);
	    }
	    return crs;
	  }
	
	
	@Override
	public void rowSetChanged(RowSetEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void rowChanged(RowSetEvent event) {
		// this updates the CRS to the next(?current?) row. 
		CachedRowSet currentRowSet = InventoryTable.empRowSet;

		try {
			currentRowSet.moveToCurrentRow();
			empTable = new InventoryTable(InventoryTable.empRowSet);
			table.setModel(empTable);

		} catch (SQLException ex) {

			DatabaseEngine.printSQLException(ex);

			// Display the error in a dialog box.

			JOptionPane.showMessageDialog(InventoryFrame.this, new String[] { // Display
																				// a
																				// 2-line
																				// message
					ex.getClass().getName() + ": ", ex.getMessage() });
		}
	}

	@Override
	public void cursorMoved(RowSetEvent event) {
		// TODO Auto-generated method stub

	}

}
