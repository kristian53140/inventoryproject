//This is only because the project source files are put within the net\bluespurs\com folders within the project directory

// This keeps my files talking to each other
package net.bluespurs.com;

//This is needed since the program has the potential to throw an exception error
import java.sql.SQLException;

public class Main {

	public static void main(String[] args) throws SQLException {
		DatabaseEngine engine = DatabaseEngine.getInstance(); // Sets initial DB connection instance
 
		
		try {
			InventoryFrame empFrame = new InventoryFrame(engine); //Sets up Frame to be displayed. Check out CourseFrame class for more info.
			empFrame.pack();
			empFrame.setVisible(true); //Displays Frame

		} catch (SQLException sqle) {
			DatabaseEngine.printSQLException(sqle);
		} catch (Exception e) {
			System.out.println("Unexpected exception");
			e.printStackTrace();
		}

	}
}
